# Media-Server mit `minidlna` im Docker-Container

Im Docker-Container läuft [MiniDLNA][1], das Medien wie Musik, Bilder und Videos über UPnP DLNA im LAN verfügbar macht. NB: Inzwischen nennt es sich eigentlich ReadyMedia, aber die Pakete bei Debian, Arch und Alpine heißen noch immer `minidlna`.

Der MiniDLNA-Container stellt **ein** Volume zur Verfügung, an das **ein** Medien-Verzeichnis angedockt werden kann. Das muss so deutlich erwähnt werden, da MiniDLNA grundsätzlich viele Verzeichnisse als Quellen einbinden kann. Das ist bei diesem Container nicht möglich.


## Bauen und Starten

```
sudo docker build -t sniner/minidlna .
```

Der Container nutzt zwei Volumes:

* `/srv/media` ist die Andockstelle für die Medien-Daten. Hier muss man immer etwas einklinken, außer man freut sich über ein leeres Inventar.
* In `/srv/db` wird die Inventar-Datenbank von MiniDLNA abgelegt. Es ist sinnvoll, ein persistentes Volume dafür zu nehmen, dann muß nicht bei jedem Start des Containers erneut inventarisiert werden.

Auf der Netzwerkseite benötigt MiniDLNA zwei Ports:

* Die Verbindung zu den Clients findet auf Port 8200/tcp statt. Dieser Port kann geändert werden.
* Über 1900/udp findet das UPnP-Discovery statt.

Das Discovery funktioniert nicht mit einer Bridge, deswegen `--net=host`. Aus dem gleichen Grund kann man sich die Port-Forwards `-p 8200:8200 -p 1900:1900/udp` sparen.

Der Start sieht also so oder ähnlich aus:

```
$ sudo docker run --rm --name=minidlna --net=host \
    -v /media/musik:/srv/media \
    -v minidlna-db:/srv/db \
    sniner/minidlna \
    friendly_name=musicbox
```

Im Beispiel ist die Musik-Bibliothek unter `/media/musik` gemountet. Die Datenbank landet im persistenten Volume `minidlna-db`. Die Clients zeigen den Namen `musicbox` an.

Wie man am Beispiel sieht, können Optionen von MiniDLNA als Argumente übergeben werden. Prinzipiell können alle Optionen, die in [`man minidlna.conf`][2] beschrieben sind, darüber gesetzt werden. Manche Optionen ergeben in diesem Zusammenhang allerdings keinen Sinn und werden daher ignoriert, zum Beispiel `db_dir` oder `log_dir`. Mit dem Argument `rescan` wird beim Start ein kompletter Rescan der Mediendateien durchgeführt.

Für einen ersten Testlauf kann man einfach das Skript `demo_install.sh` anpassen und starten. Im Test-Skript wird der Log-Level auf `debug` gesetzt, das erzeugt eine Menge Ausgaben, aber man sieht wenigstens was los ist. Im Normalfall werden nur Benachrichtigungen ab Level `warn` ins Log geschrieben.


## Mehrere Instanzen

Man kann mehr als einen MiniDLNA-Container auf dem selben Host laufen lassen. Das ist zum Beispiel interessant, um Inhalte zu trennen. Hier ein abgekürztes Beispiel:

```
$ sudo docker run --rm --name=music -v /media/musik:/srv/media ... sniner/minidlna port=8200 uuid=$(uuidgen)
$ sudo docker run --rm --name=video -v /media/video:/srv/media ... sniner/minidlna port=8201 uuid=$(uuidgen)
```

Dabei ist wichtig:

* Jeder Container benötigt einen eigenen TCP-Port. Im Beispiel 8200 und der zweite Container 8201. Da im `Dockerfile` nur 8200 exportiert wird, funktioniert das nur mit `--net=host` ... allerdings ist das schon wegen UPnP erforderlich.
* Verschiedene Instanzen werden durch ihre UUID unterschieden. Die UUID wird standardmäßig von der MAC-Adresse abgeleitet und wäre somit bei mehreren Instanzen auf dem gleichen Host identisch. Dadurch erkennt ein Client nur eine Instanz. Man muss daher jeder Instanz mit `uuid=xxxx` eine eigene, eindeutige UUID mitgeben. Diese lässt sich mit `uuidgen` einfach erzeugen. Für den Produktivbetrieb würde man diese einmalig ermitteln und nicht bei jedem Start neu.

Die UUID wird im Datenbank-Volume abgelegt und wiederverwendet, sofern keine UUID beim Start des Containers mitgegeben wird.


## Dies und das

MiniDLNA verwendet inotify um Änderungen am Dateisystem zu beobachten. Außer man deaktiviert das mit `inotify=no`. Bei einer großen Medienbibliothek kann es nötig sein, das Limit für inotify anzuheben:

```
$ cat /proc/sys/fs/inotify/max_user_watches
$ echo 100000 > /proc/sys/fs/inotify/max_user_watches
```

[1]: https://sourceforge.net/projects/minidlna/
[2]: https://www.mankier.com/5/minidlna.conf
[3]: https://github.com/bobrik/docker-minidlna
