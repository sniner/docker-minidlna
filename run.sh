#!/bin/bash

set -e

cp /etc/minidlna.conf.template /etc/minidlna.conf

[[ -r /srv/db/uuid ]] && uuid=$(< /srv/db/uuid) || uuid=$(/usr/bin/uuidgen)

opts=""
for var in "$@"; do
    if [[ ${var} =~ ^([^=]{1,})=(.*) ]]; then
        key=${BASH_REMATCH[1],,}
        val=${BASH_REMATCH[2]}
        case "${key}" in
            db_dir|log_dir)
                echo "Protected option: ${key}"
                ;;
            uuid)
                uuid="${val}"
                ;;
            *)
                echo ${key}=${val} >> /etc/minidlna.conf
                ;;
        esac
    else
        case "${var,,}" in
            rescan)
                opts="${opts} -R"
                ;;
            *)
                echo "Unknown command: ${var}"
                ;;
        esac
    fi
done

echo "uuid=${uuid}" >> /etc/minidlna.conf
echo -n "uuid" > /srv/db/uuid

chown -R minidlna:minidlna /srv/db
chmod 0755 /srv/db

mkdir -p /var/run/minidlna
chown minidlna:minidlna /var/run/minidlna
chmod 0700 /var/run/minidlna

exec /sbin/su-exec minidlna:minidlna /usr/sbin/minidlnad -S $opts
