sudo docker build -t sniner/minidlna .
sudo docker run --rm \
    --name=minidlna \
    --net=host \
    -v /media/music:/srv/media \
    -v minidlna-db:/srv/db \
    sniner/minidlna \
    friendly_name=musicbox \
    rescan \
    log_level=general,artwork,database,inotify,scanner,metadata,http,ssdp,tivo=debug
