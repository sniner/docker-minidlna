FROM alpine:latest
LABEL maintainer="Stefan Schönberger <mail@sniner.net>"

# Pakete installieren
RUN apk update && \
    apk --no-cache add \
        bash \
        su-exec \
        minidlna \
        util-linux \
    && rm -rf /var/cache/apk/*

# Konfigurationsdatei kopieren
COPY ./minidlna.conf.template /etc

# Volumes für DB und Medien
VOLUME ["/srv/media"]
VOLUME ["/srv/db"]

# Diese Ports müssen von außen erreichbar sein
EXPOSE 8200 1900/udp

# Das Log soll nicht im Container verrotten
RUN ln -sf /dev/stdout /var/log/minidlna.log

# Startskript ins Image kopieren
COPY ./run.sh /
RUN chmod +x /run.sh

ENTRYPOINT ["/run.sh"]
